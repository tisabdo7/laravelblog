@extends('layouts.app')

@section('content')
    <div class="background-image grid grid-cols-1 m-auto">
        <div class="flex text-gray-100 pt-10">
            <div class="m-auto pt-4 pb-16 sm:m-auto w-4/5 block text-center">
                <h1 class="sm:text-white text-5xl uppercase font-bold text-shadow-md pb-14">
                      Blog
                </h1>
                <p 
                    
                    class="text-center bg-gray-50 text-gray-700 py-2 px-4  text-xl uppercase">
                    In a fast-paced world, we believe in the power of pausing to reflect on everyday details. Our blog offers a diverse range of articles on topics like technology, science, art, culture, health, nutrition, travel, and adventure.

Whether you're looking for inspiration, knowledge, or just an enjoyable read, our blog is your perfect destination. Join our community of passionate learners and creative thinkers.

Thank you for visiting, and we look forward to having you as part of our journey!
                </p>
            </div>
        </div>
    </div>

    <div class="sm:grid grid-cols-2 gap-20 w-4/5 mx-auto py-15 border-b border-gray-200">
        <div>
            <img src="https://cdn.pixabay.com/photo/2014/05/03/01/03/laptop-336704_960_720.jpg" width="700" alt="">
        </div>

        <div class="m-auto sm:m-auto text-left w-4/5 block">
            <h2 class="text-3xl font-extrabold text-gray-600 pb-14">
                Struggling to be a better web developer?
            </h2>
            

            <a 
                href="/blog"
                class="uppercase bg-blue-500 text-gray-100 text-s font-extrabold py-3 px-8 rounded-3xl">
                Find Out More
            </a>
        </div>
    </div>



    <div class="text-center py-15">
        <span class="uppercase text-s text-gray-400">
            Blog
        </span>

        <h2 class="text-4xl font-bold py-10">
            Recent Posts
        </h2>

        <p class="m-auto w-4/5 text-gray-500">
            In a fast-paced world, we believe in the power of pausing to reflect on everyday details. Our blog offers a diverse range of articles on topics like technology, science, art, culture, health, nutrition, travel, and adventure.

            Whether you're looking for inspiration, knowledge, or just an enjoyable read, our blog is your perfect destination. Join our community of passionate learners and creative thinkers.
            
            Thank you for visiting, and we look forward to having you as part of our journey!
        </p>
    </div>

  
@endsection